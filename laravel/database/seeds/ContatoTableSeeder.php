<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'nome' => 'Lidiane Lourenço',
            'email' => 'contato@trupe.net',
            'telefone' => '+55 11 98101 4603',
            'endereco' => 'Rua Bacaetava, 191 · sala 1603',
            'bairro' => 'Brooklin',
            'cidade' => 'São Paulo - SP',
            'instagram' => 'https://www.instagram.com/llccdesign/',
            'imagem' => '',
        ]);
    }
}
