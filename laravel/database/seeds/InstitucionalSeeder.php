<?php

use Illuminate\Database\Seeder;

class InstitucionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('institucional')->insert([
            'imagem' => '',
            'texto' => '',
        ]);
    }
}
