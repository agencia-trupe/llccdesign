<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title' => 'llcc Design',
            'description' => '',
            'keywords' => '',
            'imagem_de_compartilhamento' => '',
            'analytics' => '',
        ]);
    }
}
