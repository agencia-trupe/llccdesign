<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'        => 'required',
            'email'       => 'email|required',
            'telefone'    => 'required',
            'cidade'      => 'required',
            'instagram'   => 'required',
            'imagem'      => 'image'
        ];
    }
}
