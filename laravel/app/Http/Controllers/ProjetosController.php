<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Projeto;
use App\Models\ProjetoImagem;

class ProjetosController extends Controller
{
    public function index()
    {
        $projetos = Projeto::with('imagens')->ordenados()->get();

        return view('frontend.projetos', compact('projetos'));
    }
}
