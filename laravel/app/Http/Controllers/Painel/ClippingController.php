<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;

class ClippingController extends Controller
{
    public function index()
    {
        $registros = Clipping::ordenados()->get();

        return view('painel.clipping.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.clipping.create');
    }

    public function store(ClippingRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Clipping::upload_capa();

            Clipping::create($input);

            return redirect()->route('painel.clipping.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);
        }
    }

    public function edit(Clipping $registro)
    {
        return view('painel.clipping.edit', compact('registro'));
    }

    public function update(ClippingRequest $request, Clipping $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Clipping::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.clipping.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Clipping $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.clipping.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
