<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Clipping;

class ClippingController extends Controller
{
    public function index()
    {
        $clipping = Clipping::with('imagens')->ordenados()->get();

        return view('frontend.clipping.index', compact('clipping'));
    }
}
