<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Institucional;

class PerfilController extends Controller
{
    public function institucional()
    {
        $institucional = Institucional::first();

        return view('frontend.perfil', compact('institucional'));
    }
}
