<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }

}
