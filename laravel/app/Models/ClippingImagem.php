<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ClippingImagem extends Model
{
    protected $table = 'clipping_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeClipping($query, $id)
    {
        return $query->where('clipping_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/clipping/imagens/thumbs/'
            ],
            [
                'width'   => 1280,
                'height'  => null,
                'upsize'  => true,
                'path'    => 'assets/img/clipping/imagens/'
            ]
        ]);
    }
}
