<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Clipping extends Model
{
    protected $table = 'clipping';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ClippingImagem', 'clipping_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'       => 300,
            'height'      => null,
            'transparent' => true,
            'path'        => 'assets/img/clipping/'
        ]);
    }
}
