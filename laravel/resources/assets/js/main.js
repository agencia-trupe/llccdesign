import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(document).ready(function () {
    $(".banners").cycle({
        slides: ".banner",
    });

    // FANCYBOX - PROJETOS E CLIPPING
    $(".fancybox").fancybox({
        padding: 0,
        prevEffect: "fade",
        nextEffect: "fade",
        closeBtn: false,
        openEffect: "elastic",
        openSpeed: "150",
        closeEffect: "elastic",
        closeSpeed: "150",
        helpers: {
            title: {
                type: "outside",
                position: "top",
            },
            overlay: {
                css: {
                    background: "rgba(132, 134, 136, .88)",
                },
            },
        },
    });

    $(".projeto").click(function handler(event) {
        event.preventDefault();

        const id = $(this).data("projeto-id");

        if (id) {
            $(`a[rel=galeria-${id}]`)[0].click();
        }
    });

    $(".clipping-galeria").click(function (e) {
        e.preventDefault();

        var galeria = $(this).data("galeria");

        $(".fancybox[rel=galeria-" + galeria)
            .first()
            .trigger("click");
    });

    // GRID CLIPPING
    var $grid = $(".clipping-thumbs")
        .masonry({
            itemSelector: ".grid-item",
            columnWidth: ".grid-item",
            percentPosition: true,
        })
        .on("layoutComplete", function (event, laidOutItems) {
            $(".clipping-thumbs").masonry("layout");
        });
        $grid.imagesLoaded().progress( function() {
            $grid.masonry('layout');
        });
    

    // BTN VER MAIS - CLIPPING
    var itensClipping = $(".clipping-itens");
    var spliceItensQuantidade = 20;

    if (itensClipping.length <= spliceItensQuantidade) {
        $(".btn-clipping").hide();
    }

    var setDivClipping = function () {
        var spliceItens = itensClipping.splice(0, spliceItensQuantidade);
        $(".clipping-thumbs").append(spliceItens);
        $(spliceItens).show();
        $(".clipping-thumbs").masonry("layout");
        if (itensClipping.length <= 0) {
            $(".btn-clipping").hide();
        }
    };

    $(".btn-clipping").click(function () {
        setDivClipping();
    });

    $(".clipping-itens").hide();
    setDivClipping();
});