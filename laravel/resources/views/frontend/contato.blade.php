@extends('frontend.common.template')

@section('content')

<div class="content contato">

    <form action="{{ route('contato.post') }}" method="POST">
        {!! csrf_field() !!}
        <div class="informacoes">
            <a href="https://api.whatsapp.com/send?phone={{ $contato->telefone }}" class="telefone" target="_blank">{{ $contato->telefone }}</a>
            <a href="mailto:{{ $contato->email }}" class="email" target="_blank">{{ $contato->email }}</a>
            <p>{{ $contato->endereco }}</p>
            <p>{{ $contato->bairro }} · {{ $contato->cidade }}</p>
        </div>

        <div class="dados">
            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
        </div>
        <div class="mensagem">
            <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
        </div>

        <button type="submit">
            <img src="{{ asset('assets/img/layout/seta-direita.svg') }}" alt="">
        </button>

        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            Mensagem enviada com sucesso!
        </div>
        @endif
    </form>
</div>

@endsection

@section('fullWidthContent')

<div class="content contato-img">
    <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
</div>

@endsection