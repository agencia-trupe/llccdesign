@extends('frontend.common.template')

@section('content')

<div class="projetos">
    <div class="center">
        @php
            $count = 0;
            $right = true;
        @endphp
        @foreach($projetos as $projeto)
            @if($count == 0)
                @if($right)
                <div class="row projetos-right">
                @else
                <div class="row projetos-left">
                @endif
            @endif
        @php
            $count++;
        @endphp
                    <a href="#" class="projeto" data-projeto-id="{{ $projeto->id }}">
                        <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                        <div class="overlay">
                            <span>{{ $projeto->titulo }}</span>
                        </div>
                    </a>
                @if($count == 3)
                </div>
                @php
                    $count = 0;
                    $right = !$right;
                @endphp
                @endif
        @endforeach
        </div>
    </div>

    <div class="projetos-show" style="display:none">
        @foreach($projetos as $projeto)
        @foreach($projeto->imagens as $imagem)
        <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $projeto->id }}" title="{{ $projeto->titulo }} ∙ {{ $projeto->ano }}"></a>
        @endforeach
        @endforeach
    </div>

    @endsection