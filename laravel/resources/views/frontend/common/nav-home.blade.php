<div class="itens-menu">
    <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>Home</a>
    <a href="{{ route('perfil') }}" @if(Tools::routeIs('perfil*')) class="active" @endif>Perfil</a>
    <a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos')) class="active" @endif>Projetos</a>
    <a href="{{ route('clipping') }}" @if(Tools::routeIs('clipping')) class="active" @endif>Clipping</a>
    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
    @if($contato->instagram)
    <div class="social">
        @if($contato->instagram)
        <a href="{{ $contato->instagram }}" target="_blank" class="instagram"></a>
        @endif
    </div>
    @endif
</div>