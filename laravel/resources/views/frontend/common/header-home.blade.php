    <header>
        <a href="{{ route('home') }}" class="logo-home">{{ config('app.name') }}</a>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines-home"></span>
        </button>
        <nav class="nav-home">
            @include('frontend.common.nav-home')
        </nav>
    </header>
