    <footer>
        <div class="footer">

            <p class="contato-cidade">{{ $contato->cidade}}</p>

            <div class="contatos">
                <p class="contato-telefone">{{ $contato->telefone }}</p>
                <p class="contato-nome">{{ $contato->nome }}</p>
            </div>

            <p class="instagram">
                <img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="">
                <a href="{{ $contato->instagram }}" target="_blank">@llccdesign</a>
            </p>

            <ul class="footer-menu1">
                <li><a href="{{ route('home') }}">_ HOME</a></li>
                <li><a href="{{ route('perfil') }}">_ PERFIL</a></li>
                <li><a href="{{ route('projetos') }}">_ PROJETOS</a></li>
            </ul>
            <ul class="footer-menu2">
                <li><a href="{{ route('clipping') }}">_ CLIPPING</a></li>
                <li><a href="{{ route('contato') }}">_ CONTATO</a></li>
            </ul>

            <p>
                <img src="{{ asset('assets/img/layout/marca-llcc-rodape.png') }}" alt="">
            </p>

            <div class="footer-info">
                <div>
                    <p>© {{ date('Y') }} {{ config('app.name') }} &middot;</p>
                    <p>Todos os direitos reservados</p>
                </div>
                <div>
                    <p><a href="http://www.trupe.net" target="_blank">Criação de Sites:</a></p>
                    <p><a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a></p>
                </div>
            </div>


        </div>
    </footer>