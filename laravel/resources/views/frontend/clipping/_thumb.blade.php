@if($clip->link)
<a href="{{ $clip->link }}" class="clipping-link clipping-itens" target="_blank">
    <div class="grid-item">
        <img src="{{ asset('assets/img/clipping/'.$clip->capa) }}" alt="" class="grid-link">
        <span>{{ $clip->titulo }}</span>
    </div>
</a>
@else
<a href="#" class="clipping-galeria clipping-itens" data-galeria="{{ $clip->id }}">
    <div class="grid-item">
        <img src="{{ asset('assets/img/clipping/'.$clip->capa) }}" alt="" class="grid-img">
        <span>{{ $clip->titulo }}</span>
    </div>
</a>
@endif