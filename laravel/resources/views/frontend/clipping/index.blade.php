@extends('frontend.common.template')

@section('content')

<div class="content clipping">
    <div class="clipping-thumbs">
        @foreach($clipping as $clip)
            @include('frontend.clipping._thumb')
        @endforeach
    </div>
    
    <button class="btn-clipping">Ver mais
        <img src="{{ asset('assets/img/layout/sinal-mais.svg') }}" alt="">
    </button>

    <div class="clipping-show" style="display:none">
        @foreach($clipping as $clip)
        @foreach($clip->imagens as $imagem)
        <a href="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $clip->id }}" title="{{ $clip->titulo }}"></a>
        @endforeach
        @endforeach
    </div>
</div>

@endsection