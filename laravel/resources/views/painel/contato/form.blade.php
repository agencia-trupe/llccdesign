@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('bairro', 'Bairro') !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade', 'Cidade-UF') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($contato->imagem)
    <img src="{{ url('assets/img/contato/'.$contato->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
