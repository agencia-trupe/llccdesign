@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Institucional</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.institucional.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.institucional.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
